# ![SkyMovies](https://bitbucket.org/jfbsene/skymovies/src/master/SkyMovieList/Resources/Assets.xcassets/AppIcon.appiconset/Icon-App-60x60%403x.png)

##### APP criado para teste.

### Modo de Utilização
1. Clonar o repositório
2. Rodar o camando `pod install`
3. Abrir o arquivo `SkyMovieList.xcworkspace`
4. Buildar o projeto no Simulador


### Bibliotecas Utilizadas
- [SDWebImage](https://github.com/SDWebImage/SDWebImage)
- [SwiftyJson](https://github.com/SwiftyJSON/SwiftyJSON)
- [Alamofire](https://github.com/Alamofire/Alamofire)
- [PKHUD](https://github.com/pkluz/PKHUD)


#### Prints
* [SplashScreen](https://bitbucket.org/jfbsene/skymovies/src/master/SkyMovieList/Resources/Assets.xcassets/screenshot00.imageset/screenshot00.png)
* [Main](https://bitbucket.org/jfbsene/skymovies/src/master/SkyMovieList/Resources/Assets.xcassets/screenshot01.imageset/screenshot01.png)
* [Details](https://bitbucket.org/jfbsene/skymovies/src/master/SkyMovieList/Resources/Assets.xcassets/screenshot02.imageset/screenshot02.png)