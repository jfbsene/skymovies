//
//  APIFactory.swift
//  SkyMovieList
//
//  Created by Joel Sene on 01/06/19.
//  Copyright © 2019 Joel Sene. All rights reserved.
//

import Foundation

public struct APIFactory {
    
    public static let sharedInstance = APIFactory()
    
    private init() {
    }
    
    public var movieService: MoviesService {
        return MoviesService()
    }
}
