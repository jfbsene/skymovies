//
//  APIRouter.swift
//  SkyMovieList
//
//  Created by Joel Sene on 01/06/19.
//  Copyright © 2019 Joel Sene. All rights reserved.
//

import Foundation
import Alamofire

typealias APIParams = [String : Any]
typealias Body = [String : Any]

protocol APIRouter: URLRequestConvertible {
    var token: String { get }
    var method: HTTPMethod { get }
    var encoding: ParameterEncoding? { get }
    var parameters: APIParams { get }
    var path: String { get }
    var baseUrl: String { get }
    var httpBody: APIParams { get }
}

extension APIRouter {
    
    var token: String {
        return ""
    }
    
    var encoding: Alamofire.ParameterEncoding? {
        return Alamofire.URLEncoding.default
    }
    
    var parameters: APIParams {
        return APIParams()
    }
    
    var httpBody: Body {
        return Body()
    }
    
    var baseUrl: String {
        return ""
    }
    
}
extension APIRouter {
    
    public func asURLRequest() throws -> URLRequest {
        let baseURL = NSURL(string: baseUrl)
        let mutableURLRequest = NSMutableURLRequest(url: baseURL!.appendingPathComponent(path)!)
        
        mutableURLRequest.httpMethod = method.rawValue
        
        mutableURLRequest.timeoutInterval = 15.0
        
        if !httpBody.isEmpty {
            mutableURLRequest.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
            mutableURLRequest.httpBody = try? JSONSerialization.data(withJSONObject: httpBody)
        }
        
        if (!token.isEmpty) {
            mutableURLRequest.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
        }
        
        if parameters.isEmpty {
            return mutableURLRequest as URLRequest
        } else {
            if let encoding = encoding {
                return try encoding.encode(mutableURLRequest as URLRequest, with: parameters)
            }
        }
        
        return mutableURLRequest as URLRequest
    }
}

