//
//  APIErrorHandler.swift
//  SkyMovieList
//
//  Created by Joel Sene on 01/06/19.
//  Copyright © 2019 Joel Sene. All rights reserved.
//

import Foundation

protocol APIErrorHandle {
    func handleHttpStatus(response: HTTPURLResponse?) -> Error?
}

extension APIErrorHandle {
    
    func handleHttpStatus(response: HTTPURLResponse?) -> Error? {
        
        guard let resp = response else { return nil }
        
        if resp.statusCode >= 200 && resp.statusCode <= 299 {
            return nil
        }
        
        return NSError(domain: "Sky", code: resp.statusCode, userInfo: nil)
        
    }
    
}
