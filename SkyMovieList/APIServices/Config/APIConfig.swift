//
//  APIConfig.swift
//  SkyMovieList
//
//  Created by Joel Sene on 01/06/19.
//  Copyright © 2019 Joel Sene. All rights reserved.
//

import Foundation

enum APIConfig {
    //""
    static let baseApiUrl: String = "https://sky-exercise.herokuapp.com/"
    
    enum Movies {
        static let movieList = "api/Movies"
    }
}
