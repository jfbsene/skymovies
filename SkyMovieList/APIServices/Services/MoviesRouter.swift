//
//  MoviesRouter.swift
//  SkyMovieList
//
//  Created by Joel Sene on 01/06/19.
//  Copyright © 2019 Joel Sene. All rights reserved.
//

import Foundation
import Alamofire

public enum MoviesRouter: APIRouter {
    case getMoviesList()
    
    var method: HTTPMethod {
        switch self {
        case .getMoviesList:
            return .get
        }
    }
    
    var path: String {
        switch self {
        case .getMoviesList:
           return APIConfig.Movies.movieList
        }
    }
    
    var baseUrl: String {
        switch self {
        case .getMoviesList:
            return APIConfig.baseApiUrl
        }
    }
    
    
    
}

