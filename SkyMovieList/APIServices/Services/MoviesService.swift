//
//  MoviesService.swift
//  SkyMovieList
//
//  Created by Joel Sene on 01/06/19.
//  Copyright © 2019 Joel Sene. All rights reserved.
//

import Foundation
import Alamofire

private protocol MoviesServiceProtocol {
    func getMovieList(router: MoviesRouter, completion: @escaping (Result<MovieList>) -> Void)
}

public struct MoviesService: MoviesServiceProtocol, APIServiceConfiguration, APIErrorHandle {
    func getMovieList(router: MoviesRouter, completion: @escaping (Result<MovieList>) -> Void) {
        self.manager.request(router).responseJSON { response -> Void in
            switch response.result {
            case .success(let value):
                do {
                    let dependents = try JSONDecoder().decode(MovieList.self, withJSONObject: value, options: [])
                    completion(Result.success(dependents))
                } catch {
                    completion(Result.failure(error))
                }
            case .failure(let error):
                completion(Result.failure(error))
            }
        }
    }
}
