//
//  MovieModel.swift
//  SkyMovieList
//
//  Created by Joel Sene on 31/05/19.
//  Copyright © 2019 Joel Sene. All rights reserved.
//

import Foundation

// MARK: - MovieListElement
struct MovieListElement: Codable {
    let title, overview, duration, releaseYear: String?
    let coverURL: String?
    let backdropsURL: [String]?
    let id: String?
    
    enum CodingKeys: String, CodingKey {
        case title, overview, duration
        case releaseYear = "release_year"
        case coverURL = "cover_url"
        case backdropsURL = "backdrops_url"
        case id
    }
}

typealias MovieList = [MovieListElement]

