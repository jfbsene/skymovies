//
//  AppDelegate.swift
//  SkyMovieList
//
//  Created by Joel Sene on 31/05/19.
//  Copyright © 2019 Joel Sene. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        let navBarButtonAppearance = UIBarButtonItem.appearance()
        navBarButtonAppearance.setTitleTextAttributes([NSAttributedString.Key.font: UIFont.systemFont(ofSize: 0.1), NSAttributedString.Key.foregroundColor: UIColor.clear], for: .normal)
        return true
    }

}

