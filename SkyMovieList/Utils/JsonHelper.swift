//
//  JsonHelper.swift
//  SkyMovieList
//
//  Created by Joel Sene on 31/05/19.
//  Copyright © 2019 Joel Sene. All rights reserved.
//

import Foundation
import SwiftyJSON

class JsonHelper {
    enum JsonKeys:String {
        case TITLE = "title"
        case OVERVIEW = "overview"
        case DURATION = "duration"
        case RELEASE_YEAR = "release_year"
        case COVER_URL = "cover_url"
        case BACKDROP_URL = "backdrops_url"
        case ID = "id"
    }
    
    class func getJsonByFileName(_ fileName: String) -> AnyObject?{
        var jsonObject:AnyObject?
        if let path = Bundle.main.path(forResource: fileName, ofType: "json") {
            if let jsonData = try? Data(contentsOf: URL(fileURLWithPath: path)) {
                do {
                    jsonObject = try JSONSerialization.jsonObject(with: jsonData, options: JSONSerialization.ReadingOptions.mutableContainers) as AnyObject
                } catch let jsonParseError {
                    print("Parse error = \(jsonParseError)")
                }
            }
        }
        return jsonObject
    }
    
    func readJSONFromFile(fileName: String) -> Any?
    {
        var json: Any?
        if let path = Bundle.main.path(forResource: fileName, ofType: "json") {
            do {
                let fileUrl = URL(fileURLWithPath: path)
                // Getting data from JSON file using the file URL
                let data = try Data(contentsOf: fileUrl, options: .mappedIfSafe)
                json = try? JSONSerialization.jsonObject(with: data)
            } catch let jsonError{
                print("Parse error = \(jsonError)")
            }
        }
        return json
    }
    
//    func getAllProperties(moduleName name:JsonKeys)->List<AppConfigModuleProperties>? {
//        guard let modulesList = self.getKPS()?.configModules,
//            let module = modulesList.first(where: { return $0.name == name.rawValue }), module.properties.count > 0
//            else {
//                return nil
//        }
//
//        return module.properties
//    }
}
