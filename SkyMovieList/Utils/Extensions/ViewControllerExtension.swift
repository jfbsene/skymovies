//
//  ViewControllerExtension.swift
//  SkyMovieList
//
//  Created by Joel Sene on 31/05/19.
//  Copyright © 2019 Joel Sene. All rights reserved.
//

import Foundation
import UIKit

extension MovieListViewController {
    
    func preload() {
        _ = self.view
    }
    
}
