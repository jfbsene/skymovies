//
//  UITableView.swift
//  SkyMovieList
//
//  Created by Joel Sene on 01/06/19.
//  Copyright © 2019 Joel Sene. All rights reserved.
//

import UIKit

extension UITableView {
    func registerTableViewCell<T: ReusableCellIdentifiable>(_ cell: T.Type) {
        register(
            UINib(
                nibName: String(describing: cell),
                bundle: nil
            ),
            forCellReuseIdentifier: T.reusableIdentifier
        )
    }
}
