//
//  UIStoryboard.swift
//  SkyMovieList
//
//  Created by Joel Sene on 01/06/19.
//  Copyright © 2019 Joel Sene. All rights reserved.
//

import UIKit

extension UIStoryboard {
    class func getViewController<T: UIViewController>(_ viewController: T.Type) -> T {
        let storyboard = UIStoryboard(name: String(describing: viewController).replace("ViewController", withString: ""), bundle: nil)
        
        return storyboard.instantiateViewController(withIdentifier: String(describing: viewController)) as! T
    }
}
