//
//  MovieListViewController.swift
//  SkyMovieList
//
//  Created by Joel Sene on 31/05/19.
//  Copyright © 2019 Joel Sene. All rights reserved.
//

import UIKit
import PKHUD

class MovieListViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    var moviesModel: MovieList = []
    var movieDetail: MovieListElement?
    private let presenter = MovieListPresenter()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.draw()
        self.presenter.attachView(view: self)
        self.presenter.requestData()
    }
    
    private func draw() {
        tableView.registerTableViewCell(HeaderTableViewCell.self)
        tableView.registerTableViewCell(MoviesTableViewCell.self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showDetails" {
            let controller = segue.destination as! MovieDetailsViewController
            controller.movieDetail = self.movieDetail
        }
    }
}

extension MovieListViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return presenter.getHeightForRow(at: indexPath)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return presenter.getNumberOfSections()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.numberOfRowsInSection(section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return presenter.cellForRowAt(tableView, indexPath: indexPath)
    }
}

extension MovieListViewController: MovieListView {
    func successScene() {
        HUD.hide(animated: true)
        self.tableView.reloadData()
    }
    
    func errorScene() {
        HUD.hide(animated: true) { (animated) in
            self.presenter.getMockedJSON("Movies")
        }
        
        self.tableView.reloadData()
    }
    
    func loadingScene() {
        HUD.show(.progress)
        self.tableView.reloadData()
    }
    
    func pushToDetails(data: MovieListElement?) {
        self.movieDetail = data
        performSegue(withIdentifier: "showDetails", sender: self)
    }
}
