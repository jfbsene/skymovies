//
//  MovieListPresenter.swift
//  SkyMovieList
//
//  Created by Joel Sene on 01/06/19.
//  Copyright © 2019 Joel Sene. All rights reserved.
//

import UIKit

class MovieListPresenter {
   
    public enum MovieListSection: Int {
        case header
        case movieList
        
    }
    
    var sections = [MovieListSection]()
    weak private var movieListView: MovieListView?
    var movieListModel = [MovieListElement]()
    private var dispatchGroup = DispatchGroup()
    let headerTitle = "Uma Seleção de Filmes Imperdíveis"
    
    //Initialize each section here
    init() {
        self.sections.append(.header)
        self.sections.append(.movieList)
    }
    
    func attachView(view: MovieListView) {
        self.movieListView = view
    }
    
    func detachView(view: MovieListView) {
        self.movieListView = nil
    }
    
    //Add API calls here
    func requestData() {
        self.getMovieList()
    }
    
    private func errorHandler() {
        movieListView?.errorScene()
    }
    
}

//MARK: TableView setup
extension MovieListPresenter {
    func getNumberOfSections() -> Int {
        return sections.count
    }
    
    func getHeightForRow(at indexPath: IndexPath)-> CGFloat {
        switch MovieListSection(rawValue: indexPath.section)! {
        case .movieList:
            return 750
        case .header:
            return 44
        }
    }
    
    func numberOfRowsInSection(_ section: Int)-> Int {
        switch MovieListSection(rawValue: sections[section].rawValue)! {
        case .header:
            return numberOfRowsInHeaderSection()
        case .movieList:
            return numberOfRowsInMovieListSection()
        }
    }
    
    func numberOfRowsInHeaderSection()-> Int {
        return 1
    }
    
    func numberOfRowsInMovieListSection()-> Int {
            return movieListModel.count - 11
        }
    
    func cellForRowAt(_ tableView: UITableView, indexPath: IndexPath)-> UITableViewCell {
        var cell = UITableViewCell()
        switch MovieListSection(rawValue: sections[indexPath.section].rawValue)! {
        case  .movieList:
          cell = self.moviesCell(tableView, indexPath: indexPath, model: movieListModel)
        case .header:
            cell = self.moviesHeader(tableView, indexPath: indexPath)
        }
        return cell
    }
    
    func moviesHeader(_ tableView: UITableView, indexPath: IndexPath)-> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: HeaderTableViewCell.reusableIdentifier) as! HeaderTableViewCell
        cell.setCell(title: headerTitle)
        return cell
    }
    
    func moviesCell(_ tableView: UITableView, indexPath: IndexPath, model: [MovieListElement])-> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: MoviesTableViewCell.reusableIdentifier) as! MoviesTableViewCell
        print(model)
        cell.setupCell(data: model, delegate: self)
        return cell
    }
}

//MARK: - API Calls
extension MovieListPresenter {
    //API
    func getMovieList() {
        self.movieListView?.loadingScene()
        
        //Check if has internet connection
        if !Reachability.isConnectedToNetwork() {
            self.errorHandler()
        }
        let service = APIFactory.sharedInstance.movieService
        let router = MoviesRouter.getMoviesList()
        
        service.getMovieList(router: router) { [weak self] (result: Result<MovieList>) in
            switch result {
            case .success(let results):
                for res in results {
                    self!.movieListModel.append(res)
                }
                self?.movieListView?.successScene()
            case .failure(let error):
                print(error.localizedDescription)
                self?.movieListView?.errorScene()
                
            }
        }
    }
    
    //Mocked JSON
    func getMockedJSON(_ fileName: String) {
        if let path = Bundle.main.path(forResource: fileName, ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .alwaysMapped)
                let jList = try JSONDecoder().decode([MovieListElement].self, from: data)
                self.movieListModel = jList
                self.movieListView?.successScene()
                print(jList)
            } catch let error {
                print("parse error: \(error.localizedDescription)")
            }
        } else {
            print("Invalid filename/path.")
        }
    }
}

//MARK: - MoviesTableViewCellDelegate
extension MovieListPresenter: MoviesTableViewCellDelegate {
    func pushToDetails(data: MovieListElement?) {
        self.movieListView?.pushToDetails(data: data)
    }
}

