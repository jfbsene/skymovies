//
//  MovieListView.swift
//  SkyMovieList
//
//  Created by Joel Sene on 01/06/19.
//  Copyright © 2019 Joel Sene. All rights reserved.
//

import Foundation

protocol MovieListView: NSObjectProtocol {
    func successScene()
    func errorScene()
    func loadingScene()
    
    func pushToDetails(data: MovieListElement?)
}

