//
//  MovieDetailsViewController.swift
//  SkyMovieList
//
//  Created by Joel Sene on 01/06/19.
//  Copyright © 2019 Joel Sene. All rights reserved.
//

import UIKit

class MovieDetailsViewController: UIViewController {
    var movieDetail: MovieListElement!
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var releaseDate: UILabel!
    @IBOutlet weak var duration: UILabel!
    @IBOutlet weak var overview: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView(detail: movieDetail)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
            self.navigationItem.title = movieDetail.title?.uppercased()
            self.navigationController?.navigationBar.tintColor = #colorLiteral(red: 0.6396254209, green: 0.7896863, blue: 1, alpha: 1)
    }
    
    func setupView(detail: MovieListElement) {
        releaseDate.text = "Ano de Lançamento: \(detail.releaseYear!)"
        duration.text = "Duração: \(detail.duration!)"
        overview.text = "Sinopse: \(detail.overview!)"
        ImageService.getImage(withURL: URL(string: detail.coverURL!)!) { image in
            ImageService.getImage(withURL: URL(string: detail.backdropsURL![0])!, completion: { backUpImage in
                self.imageView.image = image ?? backUpImage
            })
        }
    }
}
