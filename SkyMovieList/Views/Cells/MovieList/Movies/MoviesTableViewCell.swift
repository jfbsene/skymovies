//
//  MoviesTableViewCell.swift
//  SkyMovieList
//
//  Created by Joel Sene on 01/06/19.
//  Copyright © 2019 Joel Sene. All rights reserved.
//

import UIKit

protocol MoviesTableViewCellDelegate {
    func pushToDetails(data: MovieListElement?)
}

class MoviesTableViewCell: UITableViewCell {
    
    @IBOutlet weak var collectionView: UICollectionView!
    private var cellDelegate: MoviesTableViewCellDelegate?
    var moviesArray = [MovieListElement]()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        collectionView.register(UINib(nibName: "MoviesCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: MoviesCollectionViewCell.reusableIdentifier)
        
    }
    
    func setupCell(data: MovieList, delegate: MoviesTableViewCellDelegate? = nil) {
        cellDelegate = delegate
        moviesArray = data
        print(moviesArray)
    }
    
}

extension MoviesTableViewCell: UICollectionViewDelegate, UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return moviesArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MoviesCollectionViewCell", for: indexPath) as! MoviesCollectionViewCell
        let indexedList = moviesArray[indexPath.row]
        cell.setCell(data: indexedList)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        switch collectionView.cellForItem(at: indexPath) {
        case is MoviesCollectionViewCell:
            let movieDetail = self.getSelectedDependentWith(indexPath)
            self.cellDelegate?.pushToDetails(data: movieDetail)
        default:
            break
        }
    }
    
    func getSelectedDependentWith(_ indexpath:IndexPath)->MovieListElement {
        return self.moviesArray[indexpath.row]
    }
}
