//
//  MoviesCollectionViewCell.swift
//  SkyMovieList
//
//  Created by Joel Sene on 31/05/19.
//  Copyright © 2019 Joel Sene. All rights reserved.
//

import UIKit
import SDWebImage

class MoviesCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var imageView: UIImageView! {
        didSet {
            self.imageView.layer.cornerRadius = 4
        }
    }
    @IBOutlet weak var title: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func setCell(data: MovieListElement) {
        ImageService.getImage(withURL: URL(string: data.coverURL ?? data.backdropsURL![0])!) { image in
            ImageService.getImage(withURL: URL(string: (data.backdropsURL?[0])!)!, completion: { imageBackUp in
                self.imageView.image = image ?? imageBackUp
            })
        }
        
        title.text = data.title
    }
}
