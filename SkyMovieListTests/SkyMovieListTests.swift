//
//  SkyMovieListTests.swift
//  SkyMovieListTests
//
//  Created by Joel Sene on 31/05/19.
//  Copyright © 2019 Joel Sene. All rights reserved.
//

import XCTest
@testable import SkyMovieList

class SkyMovieListTests: XCTestCase {
    
    var viewController: MovieListViewController!
    
    override func setUp() {
        super.setUp()
        viewController = UIStoryboard(name: "MovieList", bundle: nil).instantiateViewController(withIdentifier: "Controller") as? MovieListViewController
        _ = viewController.view
    }
    
    func testControllerHasCollectionView() {
        viewController.loadViewIfNeeded()
        
        XCTAssertNotNil(viewController.tableView,
                        "Controller should have a tableview")
        XCTAssertNotNil(viewController.moviesModel,
                        "Data Source should have data")
    }

    override func tearDown() {
        super.tearDown()
    }

}
